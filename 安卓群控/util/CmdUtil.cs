﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adb群控.util
{
    class CmdUtil
    {

        public  String excute(String cmd)
        {

            try
            {
                Debug.WriteLine("==========cmd执行命令："+cmd);
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {
                    process.StartInfo.FileName = "cmd.exe";
                    process.StartInfo.UseShellExecute = false;     //是否使用操作系统shell启动
                    process.StartInfo.CreateNoWindow = true;        //不显示程序窗口
                    process.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
                    process.StartInfo.RedirectStandardInput = true;  //接受来自调用程序的输入信息
                    process.StartInfo.RedirectStandardError = true;  //重定向标准错误输出
                    process.Start();
                    process.StandardInput.WriteLine(cmd + "&exit");   //向cmd窗口发送输入信息，&exit意思为不论command命令执行成功与否，接下来都执行exit这句
                    process.StandardInput.AutoFlush = true;
                    string output = process.StandardOutput.ReadToEnd();  //获取cmd的输出信息
                    process.WaitForExit();         //等待程序执行完退出进程
                    process.Close();
                    process.Dispose();
                    Debug.WriteLine("========== 返回信息：" + output);
                    return output;
                }
            }
            catch (Exception a)
            {
                Debug.WriteLine(a.Message);
                return null;
            }


        }

        public void excutex(String cmd)
        {

            try
            {
                Debug.WriteLine("==========cmd执行命令：" + cmd);
                using (System.Diagnostics.Process process = new System.Diagnostics.Process())
                {
                    process.StartInfo.FileName = "cmd.exe";
                    process.StartInfo.UseShellExecute = false;     //是否使用操作系统shell启动
                    process.StartInfo.CreateNoWindow = true;        //不显示程序窗口
                    process.StartInfo.RedirectStandardOutput = true;  //由调用程序获取输出信息
                    process.StartInfo.RedirectStandardInput = true;  //接受来自调用程序的输入信息
                    process.StartInfo.RedirectStandardError = true;  //重定向标准错误输出
                    process.Start();
                    process.StandardInput.WriteLine(cmd + "&exit");   //向cmd窗口发送输入信息，&exit意思为不论command命令执行成功与否，接下来都执行exit这句
                    process.StandardInput.AutoFlush = true;
                }
                //string output = process.StandardOutput.ReadToEnd();  //获取cmd的输出信息
               // process.WaitForExit();         //等待程序执行完退出进程
               // process.Close();
               // process.Dispose();
               // Debug.WriteLine("========== 返回信息：" + output);
               // return output;
            }
            catch (Exception a)
            {
                Debug.WriteLine(a.Message);
                
            }


        }



    }
}
