﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adb群控.model
{
    class FormVo
    {
        /// <summary>
        /// x坐标
        /// </summary>
        private int x;

        /// <summary>
        /// y坐标
        /// </summary>
        private int y;

        public int X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public int Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }
    }
}
